import Tiptap from './comps/tiptap'

export const App = () => {
	return (
		<div>
			<p>tiptap editor</p>
			<div className="app">
				<Tiptap />
			</div>
		</div>
	)
}

export default App
