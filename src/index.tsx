import { render } from 'preact/compat'
import App from './app'
import { injectStyle } from './inject-style'

export const main = async () => {
	injectStyle()
	render(<App />, document.body)
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
})
